#REQUIRES -Version 2.0
Set-StrictMode -Version 2.0

<#  
    Copyright 2012 - Microsoft
#>

function Install-NAVComponent {
    <#
    .SYNOPSIS
        Installs the Microsoft Dynamics NAV binaries on the local computer.
    .DESCRIPTION
        Use the Install-NAVComponent cmdlet to install the Microsoft Dynamics NAV program files.
        Install-NAVComponent installs the program files by calling the setup program that is located in the root folder of the Microsoft Dynamics NAV installation media. This folder also contains the NavInstallationTools.psm1 PowerShell module file.
        Install-NAVComponent logs the action in the Application event log and returns when the setup program has completed.
		Check the event log and the log file that is specified in the event log entry to diagnose problems that occur during installation.

        To create a setup configuration file, run Microsoft Dynamics NAV Setup and in the Specify parameters page, choose Save.
  
        Install-NAVComponent installs the same prerequisites as the setup program and it assumes that the user is an administrator and has permission to install products on the computer.
        Install-NAVComponent takes 5-10 minutes to complete or longer if prerequisites must be installed.
	
    .PARAMETER ConfigFile
        Specifies the configuration file to use. The configuration file specifies which components to install.
        The default file is the Install-NAVComponentConfig.xml file, which is located in the root folder of the Microsoft Dynamics NAV installation media.

    .EXAMPLE
        Install-NAVComponent

        This example installs a default installation of Microsoft Dynamics NAV on the local computer.
        You must be an administrator to run the installation.
    .EXAMPLE
        Install-NAVComponent –ConfigFile C:\Temp\Temp-ConfigFile.xml

        This example installs Microsoft Dynamics NAV on the local computer according to the parameters in the setup configuration file.
        To create a setup configuration file, run Microsoft Dynamics NAV Setup and in the Specify parameters page, choose Save.
    #>
    [cmdletbinding(SupportsShouldProcess=$true)]
    param (
        [parameter(Mandatory=$false)]
		[ValidateNotNullOrEmpty()]
        [string]$ConfigFile = (Join-Path $PsScriptRoot "Install-NAVComponentConfig.xml")
    )   
    PROCESS {        
        if ($pscmdlet.ShouldProcess("ShouldProcess Install-NAVComponent") -eq $false) {
            return
        }
        
        [string]$logName = "Application"
        [int]$eventId = 0
        [int]$category = 0
        [string]$source = "Install-NAVComponent"
        [string]$navSetupExe = (Join-Path $PsScriptRoot "Setup.exe")
                        
        if ((Test-Path -PathType Leaf $ConfigFile) -eq $false)
        {
            Write-Error "The config file cannot be found: $ConfigFile"
            return
        }

        if ((Test-Path -PathType Leaf $navSetupExe) -eq $false)
        {
            Write-Error "The setup.exe file cannot be found: $navSetupExe"
            return
        }

        # get a unique log file name
        [string]$logFile = (Join-Path $env:Temp  "Install-NAVComponent.log")
        [int]$i = 0
        while (Test-Path -PathType Leaf $logFile) {
            [int]$i = $i + 1
            $logFile = (Join-Path $env:Temp  "Install-NAVComponent$i.log")
        }
 
        [string]$argumentList =  "-quiet -config `"$ConfigFile`" -log `"$logFile`""
 
        #Register the Install-NAVComponent as a source in the Application eventlog, and write that we are installing in the eventlog
        New-EventLog -LogName $logName -Source $source -ErrorAction SilentlyContinue
        Write-Eventlog -Logname $logName -Source $source -EventID $eventId -Entrytype Information -Category 0 -Message "$source started executing $navSetupExe  $argumentList"

        Start-ProcessWithProgress -FilePath $navSetupExe -ArgumentList $argumentList -TimeOutSeconds 6000

        if ((Test-Path -PathType Leaf $logFile) -eq $false)
        {
            Write-Eventlog -Logname $logName -Source $source -EventID $eventId -Entrytype Error -Category $category -Message "$source failed executing $navSetupExe"
            Write-Error "The setup program failed: $navSetupExe. The log file is not found."
            return
        }

        # We should always expect a footer to be logged, otherwise the setup process has been terminated prematurely
        [string]$logFooter = '=== Logging stopped:'
        [bool]$noFooter = (@(select-string -path $logFile -pattern $logFooter).Count -eq 0)
        if ($noFooter)
        {
            Write-Eventlog -Logname $logName -Source $source -EventID $eventId -Entrytype Error -Category $category -Message "$source failed executing $navSetupExe. The program terminated unexpectedly."
            Write-Error "The setup program terminated unexpectedly: $navSetupExe. More details can be found in the log file located on the target machine: $logFile."
            return
        }

        # the only reliable way to identify errors is searching the log file for "Error"
        [int]$errorCount = @(select-string -path $logFile -pattern "Error:").Count
        if ($errorCount -ne 0)
        {
            Write-Eventlog -Logname $logName -Source $source -EventID $eventId -Entrytype Error -Category $category -Message "$source failed executing $navSetupExe. Check $logFile for details."
            Write-Error "The setup program failed: $navSetupExe. More details can be found in the log file located on the target machine: $logFile."
            return
        }

		# Searching for "Error" does not catch UAC issues, but if the log file is small this is a symptom of missing permissions 
		[bool]$logFileIsToSmall = (get-childItem  $logFile |  Measure-object length -Sum).Sum -le 1000
        if ($logFileIsToSmall)
        {
            Write-Eventlog -Logname $logName -Source $source -EventID $eventId -Entrytype Error -Category $category -Message "$source failed executing $navSetupExe. Check $logFile for details."
            Write-Error "The setup program failed: $navSetupExe`nVerify that the user who runs the setup program is an administrator."
            return
        }

        # Setup.exe may extend 'PSModulePath' environmental variable with paths to NAV modules. Update the current session PSModulePath form machine config
        $env:PSModulePath = [Environment]::GetEnvironmentVariables("Machine").PSModulePath

		# stops and disables the default service instance (if any)
		$ServicePrefix = "MicrosoftDynamicsNavServer$"
		$SampleServiceName = "$ServicePrefix*SampleDynamicsNAV*"
		foreach($SampleService in (Get-Service -Name $SampleServiceName -ErrorAction SilentlyContinue))
		{
			Stop-Service -Name $SampleService.Name
			Set-Service -Name $SampleService.Name -StartupType Disabled
            $ServiceInstance = $SampleService.ServiceName.Replace($ServicePrefix, "")
            
            if (Get-Command Remove-NavWebServerInstance -errorAction SilentlyContinue) {
                Remove-NavWebServerInstance -InstanceName $ServiceInstance -ErrorAction SilentlyContinue 
            }
		}

        Write-Eventlog -Logname $logName -Source $source -EventID $eventId -Entrytype Information -Category $category -Message "$source succeeded executing $navSetupExe Log: $logFile"
    }
}

<#
Start a process with progress indication and timeout.
#>
function Start-ProcessWithProgress
{
    param 
        (
            [parameter(Mandatory=$true)]
            [string] $FilePath,
            [parameter(Mandatory=$true)]
            [string] $ArgumentList,
            [parameter(Mandatory=$true)]
            [int] $TimeOutSeconds
        )
    PROCESS 
    {
        $process = Start-Process -FilePath $FilePath -PassThru -ErrorAction SilentlyContinue -ArgumentList $ArgumentList

        # progress indication, upper bound TimeOutSeconds
        # so we wait $milliSecBetweenPolls which is 1% of the TimeOutSeconds between each poll
        [int]$milliSecBetweenPolls = $TimeOutSeconds * 10
        [int]$percent = 0;

        while (!$process.WaitForExit($milliSecBetweenPolls)) {
            # each iteration waits 1% of the time we increase the percentage by 1 in each iteration.
            $percent = $percent + 1;

            if ($percent -eq 100) {
                # Time's up, we did not complete and need to kill our process
                Stop-Process $process.Id -Force
                Write-Error "The setup program did not complete within the expected time, the setup was aborted."
                break
            }

            Write-Progress -Activity "Installing..." -PercentComplete $percent -CurrentOperation "$percent% complete" -Status "Please wait."
        }

        Write-Progress -Activity "Installing..." -PercentComplete 100 -Status "Done."
    }
}

# All functions are local but Install-NAVComponent in this module.
Export-ModuleMember -Function "Install-NAVComponent"
# SIG # Begin signature block
# MIIjpgYJKoZIhvcNAQcCoIIjlzCCI5MCAQExDzANBglghkgBZQMEAgEFADB5Bgor
# BgEEAYI3AgEEoGswaTA0BgorBgEEAYI3AgEeMCYCAwEAAAQQH8w7YFlLCE63JNLG
# KX7zUQIBAAIBAAIBAAIBAAIBADAxMA0GCWCGSAFlAwQCAQUABCC+soy9C3o4wz0G
# ZEvCkTb+DKoWy5vBz1ehjt1wR5CX56CCDYEwggX/MIID56ADAgECAhMzAAABA14l
# HJkfox64AAAAAAEDMA0GCSqGSIb3DQEBCwUAMH4xCzAJBgNVBAYTAlVTMRMwEQYD
# VQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNy
# b3NvZnQgQ29ycG9yYXRpb24xKDAmBgNVBAMTH01pY3Jvc29mdCBDb2RlIFNpZ25p
# bmcgUENBIDIwMTEwHhcNMTgwNzEyMjAwODQ4WhcNMTkwNzI2MjAwODQ4WjB0MQsw
# CQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9u
# ZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMR4wHAYDVQQDExVNaWNy
# b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
# AQDRlHY25oarNv5p+UZ8i4hQy5Bwf7BVqSQdfjnnBZ8PrHuXss5zCvvUmyRcFrU5
# 3Rt+M2wR/Dsm85iqXVNrqsPsE7jS789Xf8xly69NLjKxVitONAeJ/mkhvT5E+94S
# nYW/fHaGfXKxdpth5opkTEbOttU6jHeTd2chnLZaBl5HhvU80QnKDT3NsumhUHjR
# hIjiATwi/K+WCMxdmcDt66VamJL1yEBOanOv3uN0etNfRpe84mcod5mswQ4xFo8A
# DwH+S15UD8rEZT8K46NG2/YsAzoZvmgFFpzmfzS/p4eNZTkmyWPU78XdvSX+/Sj0
# NIZ5rCrVXzCRO+QUauuxygQjAgMBAAGjggF+MIIBejAfBgNVHSUEGDAWBgorBgEE
# AYI3TAgBBggrBgEFBQcDAzAdBgNVHQ4EFgQUR77Ay+GmP/1l1jjyA123r3f3QP8w
# UAYDVR0RBEkwR6RFMEMxKTAnBgNVBAsTIE1pY3Jvc29mdCBPcGVyYXRpb25zIFB1
# ZXJ0byBSaWNvMRYwFAYDVQQFEw0yMzAwMTIrNDM3OTY1MB8GA1UdIwQYMBaAFEhu
# ZOVQBdOCqhc3NyK1bajKdQKVMFQGA1UdHwRNMEswSaBHoEWGQ2h0dHA6Ly93d3cu
# bWljcm9zb2Z0LmNvbS9wa2lvcHMvY3JsL01pY0NvZFNpZ1BDQTIwMTFfMjAxMS0w
# Ny0wOC5jcmwwYQYIKwYBBQUHAQEEVTBTMFEGCCsGAQUFBzAChkVodHRwOi8vd3d3
# Lm1pY3Jvc29mdC5jb20vcGtpb3BzL2NlcnRzL01pY0NvZFNpZ1BDQTIwMTFfMjAx
# MS0wNy0wOC5jcnQwDAYDVR0TAQH/BAIwADANBgkqhkiG9w0BAQsFAAOCAgEAn/XJ
# Uw0/DSbsokTYDdGfY5YGSz8eXMUzo6TDbK8fwAG662XsnjMQD6esW9S9kGEX5zHn
# wya0rPUn00iThoj+EjWRZCLRay07qCwVlCnSN5bmNf8MzsgGFhaeJLHiOfluDnjY
# DBu2KWAndjQkm925l3XLATutghIWIoCJFYS7mFAgsBcmhkmvzn1FFUM0ls+BXBgs
# 1JPyZ6vic8g9o838Mh5gHOmwGzD7LLsHLpaEk0UoVFzNlv2g24HYtjDKQ7HzSMCy
# RhxdXnYqWJ/U7vL0+khMtWGLsIxB6aq4nZD0/2pCD7k+6Q7slPyNgLt44yOneFuy
# bR/5WcF9ttE5yXnggxxgCto9sNHtNr9FB+kbNm7lPTsFA6fUpyUSj+Z2oxOzRVpD
# MYLa2ISuubAfdfX2HX1RETcn6LU1hHH3V6qu+olxyZjSnlpkdr6Mw30VapHxFPTy
# 2TUxuNty+rR1yIibar+YRcdmstf/zpKQdeTr5obSyBvbJ8BblW9Jb1hdaSreU0v4
# 6Mp79mwV+QMZDxGFqk+av6pX3WDG9XEg9FGomsrp0es0Rz11+iLsVT9qGTlrEOla
# P470I3gwsvKmOMs1jaqYWSRAuDpnpAdfoP7YO0kT+wzh7Qttg1DO8H8+4NkI6Iwh
# SkHC3uuOW+4Dwx1ubuZUNWZncnwa6lL2IsRyP64wggd6MIIFYqADAgECAgphDpDS
# AAAAAAADMA0GCSqGSIb3DQEBCwUAMIGIMQswCQYDVQQGEwJVUzETMBEGA1UECBMK
# V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
# IENvcnBvcmF0aW9uMTIwMAYDVQQDEylNaWNyb3NvZnQgUm9vdCBDZXJ0aWZpY2F0
# ZSBBdXRob3JpdHkgMjAxMTAeFw0xMTA3MDgyMDU5MDlaFw0yNjA3MDgyMTA5MDla
# MH4xCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQHEwdS
# ZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xKDAmBgNVBAMT
# H01pY3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBIDIwMTEwggIiMA0GCSqGSIb3DQEB
# AQUAA4ICDwAwggIKAoICAQCr8PpyEBwurdhuqoIQTTS68rZYIZ9CGypr6VpQqrgG
# OBoESbp/wwwe3TdrxhLYC/A4wpkGsMg51QEUMULTiQ15ZId+lGAkbK+eSZzpaF7S
# 35tTsgosw6/ZqSuuegmv15ZZymAaBelmdugyUiYSL+erCFDPs0S3XdjELgN1q2jz
# y23zOlyhFvRGuuA4ZKxuZDV4pqBjDy3TQJP4494HDdVceaVJKecNvqATd76UPe/7
# 4ytaEB9NViiienLgEjq3SV7Y7e1DkYPZe7J7hhvZPrGMXeiJT4Qa8qEvWeSQOy2u
# M1jFtz7+MtOzAz2xsq+SOH7SnYAs9U5WkSE1JcM5bmR/U7qcD60ZI4TL9LoDho33
# X/DQUr+MlIe8wCF0JV8YKLbMJyg4JZg5SjbPfLGSrhwjp6lm7GEfauEoSZ1fiOIl
# XdMhSz5SxLVXPyQD8NF6Wy/VI+NwXQ9RRnez+ADhvKwCgl/bwBWzvRvUVUvnOaEP
# 6SNJvBi4RHxF5MHDcnrgcuck379GmcXvwhxX24ON7E1JMKerjt/sW5+v/N2wZuLB
# l4F77dbtS+dJKacTKKanfWeA5opieF+yL4TXV5xcv3coKPHtbcMojyyPQDdPweGF
# RInECUzF1KVDL3SV9274eCBYLBNdYJWaPk8zhNqwiBfenk70lrC8RqBsmNLg1oiM
# CwIDAQABo4IB7TCCAekwEAYJKwYBBAGCNxUBBAMCAQAwHQYDVR0OBBYEFEhuZOVQ
# BdOCqhc3NyK1bajKdQKVMBkGCSsGAQQBgjcUAgQMHgoAUwB1AGIAQwBBMAsGA1Ud
# DwQEAwIBhjAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFHItOgIxkEO5FAVO
# 4eqnxzHRI4k0MFoGA1UdHwRTMFEwT6BNoEuGSWh0dHA6Ly9jcmwubWljcm9zb2Z0
# LmNvbS9wa2kvY3JsL3Byb2R1Y3RzL01pY1Jvb0NlckF1dDIwMTFfMjAxMV8wM18y
# Mi5jcmwwXgYIKwYBBQUHAQEEUjBQME4GCCsGAQUFBzAChkJodHRwOi8vd3d3Lm1p
# Y3Jvc29mdC5jb20vcGtpL2NlcnRzL01pY1Jvb0NlckF1dDIwMTFfMjAxMV8wM18y
# Mi5jcnQwgZ8GA1UdIASBlzCBlDCBkQYJKwYBBAGCNy4DMIGDMD8GCCsGAQUFBwIB
# FjNodHRwOi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtpb3BzL2RvY3MvcHJpbWFyeWNw
# cy5odG0wQAYIKwYBBQUHAgIwNB4yIB0ATABlAGcAYQBsAF8AcABvAGwAaQBjAHkA
# XwBzAHQAYQB0AGUAbQBlAG4AdAAuIB0wDQYJKoZIhvcNAQELBQADggIBAGfyhqWY
# 4FR5Gi7T2HRnIpsLlhHhY5KZQpZ90nkMkMFlXy4sPvjDctFtg/6+P+gKyju/R6mj
# 82nbY78iNaWXXWWEkH2LRlBV2AySfNIaSxzzPEKLUtCw/WvjPgcuKZvmPRul1LUd
# d5Q54ulkyUQ9eHoj8xN9ppB0g430yyYCRirCihC7pKkFDJvtaPpoLpWgKj8qa1hJ
# Yx8JaW5amJbkg/TAj/NGK978O9C9Ne9uJa7lryft0N3zDq+ZKJeYTQ49C/IIidYf
# wzIY4vDFLc5bnrRJOQrGCsLGra7lstnbFYhRRVg4MnEnGn+x9Cf43iw6IGmYslmJ
# aG5vp7d0w0AFBqYBKig+gj8TTWYLwLNN9eGPfxxvFX1Fp3blQCplo8NdUmKGwx1j
# NpeG39rz+PIWoZon4c2ll9DuXWNB41sHnIc+BncG0QaxdR8UvmFhtfDcxhsEvt9B
# xw4o7t5lL+yX9qFcltgA1qFGvVnzl6UJS0gQmYAf0AApxbGbpT9Fdx41xtKiop96
# eiL6SJUfq/tHI4D1nvi/a7dLl+LrdXga7Oo3mXkYS//WsyNodeav+vyL6wuA6mk7
# r/ww7QRMjt/fdW1jkT3RnVZOT7+AVyKheBEyIXrvQQqxP/uozKRdwaGIm1dxVk5I
# RcBCyZt2WwqASGv9eZ/BvW1taslScxMNelDNMYIVezCCFXcCAQEwgZUwfjELMAkG
# A1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQx
# HjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjEoMCYGA1UEAxMfTWljcm9z
# b2Z0IENvZGUgU2lnbmluZyBQQ0EgMjAxMQITMwAAAQNeJRyZH6MeuAAAAAABAzAN
# BglghkgBZQMEAgEFAKCBzjAZBgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAcBgor
# BgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAvBgkqhkiG9w0BCQQxIgQgJnY/Gsxf
# sMZ0rLHpvnYLB7yUnEgkCCLJzpTKRWHItsowYgYKKwYBBAGCNwIBDDFUMFKgNIAy
# AE4AYQB2AEkAbgBzAHQAYQBsAGwAYQB0AGkAbwBuAFQAbwBvAGwAcwAuAHAAcwBt
# ADGhGoAYaHR0cDovL3d3dy5taWNyb3NvZnQuY29tMA0GCSqGSIb3DQEBAQUABIIB
# AEEmMT0s6FmUKYskIOJ/ymJ/bFxdERDQLpLeuvBB4K8cZXrDGm6acuzMUsxdvj6n
# HOyuQu3M+ABlcfekF8nIsLdAEAiXtJm4RN8LIal/dp/FyBmBkGikH1qjr41lS8Zs
# dwyAFoxmlMn4vFwuylqoCmtGubYACzsKWAWZvmPrTM8q1mC32aZQ9J4lAJUYV8cS
# XAHTEC8mfkYTFbX+CpG6MdcNjqQM/o5kjKDkVF0kBaJJZZ0+B+azKqQAiZfpwsAb
# MqpTiS9s4MnX+MafEdGue6jE0ibNJ/uqmD1HcEzzUapk193YQ537kCJ7TMYC+PPB
# zU21nQhuujvWMcrqVaCscK2hghLlMIIS4QYKKwYBBAGCNwMDATGCEtEwghLNBgkq
# hkiG9w0BBwKgghK+MIISugIBAzEPMA0GCWCGSAFlAwQCAQUAMIIBUQYLKoZIhvcN
# AQkQAQSgggFABIIBPDCCATgCAQEGCisGAQQBhFkKAwEwMTANBglghkgBZQMEAgEF
# AAQgiafHmTXV3Z2pKtDHc6bePit8Etj7CqPslLxfEPcNfTUCBlyVG8LHrxgTMjAx
# OTAzMjMyMzEyMTUuMDg3WjAEgAIB9KCB0KSBzTCByjELMAkGA1UEBhMCVVMxEzAR
# BgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1p
# Y3Jvc29mdCBDb3Jwb3JhdGlvbjElMCMGA1UECxMcTWljcm9zb2Z0IEFtZXJpY2Eg
# T3BlcmF0aW9uczEmMCQGA1UECxMdVGhhbGVzIFRTUyBFU046RDZCRC1FM0U3LTE2
# ODUxJTAjBgNVBAMTHE1pY3Jvc29mdCBUaW1lLVN0YW1wIFNlcnZpY2Wggg48MIIE
# 8TCCA9mgAwIBAgITMwAAAPV0GzyKFyt87QAAAAAA9TANBgkqhkiG9w0BAQsFADB8
# MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
# bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSYwJAYDVQQDEx1N
# aWNyb3NvZnQgVGltZS1TdGFtcCBQQ0EgMjAxMDAeFw0xODEwMjQyMTE0MjZaFw0y
# MDAxMTAyMTE0MjZaMIHKMQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3Rv
# bjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
# aW9uMSUwIwYDVQQLExxNaWNyb3NvZnQgQW1lcmljYSBPcGVyYXRpb25zMSYwJAYD
# VQQLEx1UaGFsZXMgVFNTIEVTTjpENkJELUUzRTctMTY4NTElMCMGA1UEAxMcTWlj
# cm9zb2Z0IFRpbWUtU3RhbXAgU2VydmljZTCCASIwDQYJKoZIhvcNAQEBBQADggEP
# ADCCAQoCggEBAMQs3tITOnmFnwHAX/C33WRDbUBT56mjv343wIBzd9D8iBcSICll
# 3zLejvucq6J3YD+CBY/DbWRL5lIZQY2TSOkjX17PWj6PK6A+lB3U85hdsF0r3zSF
# ZO5npj3YcSO85Kpz95xGdhIYMzTYcFP3qgqOawn/GF+r9WPJwfD21SO0BiVIThAp
# MFAdN97azOBgmgl44/ZA1e1/trCKjCqJpZyb2mITIeG6wMkufj4J/brMPMb2JDvF
# GfIYmOjEQ9wITtR+J/cTc8LA4GxJnusAWA9WdtPcyempd7Cl7XKP+y6V354pr2Hr
# tlwYCH/7AWbgY7RoAavbmMvalqJDaM9jjnMCAwEAAaOCARswggEXMB0GA1UdDgQW
# BBTbYHfnytdgXOUG3sNiTGrGUV5gZDAfBgNVHSMEGDAWgBTVYzpcijGQ80N7fEYb
# xTNoWoVtVTBWBgNVHR8ETzBNMEugSaBHhkVodHRwOi8vY3JsLm1pY3Jvc29mdC5j
# b20vcGtpL2NybC9wcm9kdWN0cy9NaWNUaW1TdGFQQ0FfMjAxMC0wNy0wMS5jcmww
# WgYIKwYBBQUHAQEETjBMMEoGCCsGAQUFBzAChj5odHRwOi8vd3d3Lm1pY3Jvc29m
# dC5jb20vcGtpL2NlcnRzL01pY1RpbVN0YVBDQV8yMDEwLTA3LTAxLmNydDAMBgNV
# HRMBAf8EAjAAMBMGA1UdJQQMMAoGCCsGAQUFBwMIMA0GCSqGSIb3DQEBCwUAA4IB
# AQCfpGwl33vnscZFJpUWj3AeGYpBCe95St6kRwo/JldfjgxB/k+8EuQAdltZ7ryH
# 8yiZfqP/BHgHTWvQesRxz0aa0YNT/svzkSTC9+bXQl0agHrLBW6UFRfCQpoOyYCA
# 5PT4Z+pyRMuSNoTGSm8ssDwSXn57zl1fyzOVnYo//QJ8oQYc04LiDphlHtxdB/Ba
# EDkcZnPqRhKYxZF/FCxoCfeQZeE3liQC+HEg+hGCC1XGeBflUYV9vI49qMSfNjiq
# GE7vl+uEXLjzj15uzQRtkcdHnL+XIu4rNuL+YMD/SwXtEWn33SCM5KtkRNlxzwdy
# V6hEI1tTm9IU/7wiUQwf8iiKMIIGcTCCBFmgAwIBAgIKYQmBKgAAAAAAAjANBgkq
# hkiG9w0BAQsFADCBiDELMAkGA1UEBhMCVVMxEzARBgNVBAgTCldhc2hpbmd0b24x
# EDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlv
# bjEyMDAGA1UEAxMpTWljcm9zb2Z0IFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5
# IDIwMTAwHhcNMTAwNzAxMjEzNjU1WhcNMjUwNzAxMjE0NjU1WjB8MQswCQYDVQQG
# EwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
# A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSYwJAYDVQQDEx1NaWNyb3NvZnQg
# VGltZS1TdGFtcCBQQ0EgMjAxMDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
# ggEBAKkdDbx3EYo6IOz8E5f1+n9plGt0VBDVpQoAgoX77XxoSyxfxcPlYcJ2tz5m
# K1vwFVMnBDEfQRsalR3OCROOfGEwWbEwRA/xYIiEVEMM1024OAizQt2TrNZzMFcm
# gqNFDdDq9UeBzb8kYDJYYEbyWEeGMoQedGFnkV+BVLHPk0ySwcSmXdFhE24oxhr5
# hoC732H8RsEnHSRnEnIaIYqvS2SJUGKxXf13Hz3wV3WsvYpCTUBR0Q+cBj5nf/Vm
# wAOWRH7v0Ev9buWayrGo8noqCjHw2k4GkbaICDXoeByw6ZnNPOcvRLqn9NxkvaQB
# wSAJk3jN/LzAyURdXhacAQVPIk0CAwEAAaOCAeYwggHiMBAGCSsGAQQBgjcVAQQD
# AgEAMB0GA1UdDgQWBBTVYzpcijGQ80N7fEYbxTNoWoVtVTAZBgkrBgEEAYI3FAIE
# DB4KAFMAdQBiAEMAQTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAfBgNV
# HSMEGDAWgBTV9lbLj+iiXGJo0T2UkFvXzpoYxDBWBgNVHR8ETzBNMEugSaBHhkVo
# dHRwOi8vY3JsLm1pY3Jvc29mdC5jb20vcGtpL2NybC9wcm9kdWN0cy9NaWNSb29D
# ZXJBdXRfMjAxMC0wNi0yMy5jcmwwWgYIKwYBBQUHAQEETjBMMEoGCCsGAQUFBzAC
# hj5odHRwOi8vd3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL01pY1Jvb0NlckF1
# dF8yMDEwLTA2LTIzLmNydDCBoAYDVR0gAQH/BIGVMIGSMIGPBgkrBgEEAYI3LgMw
# gYEwPQYIKwYBBQUHAgEWMWh0dHA6Ly93d3cubWljcm9zb2Z0LmNvbS9QS0kvZG9j
# cy9DUFMvZGVmYXVsdC5odG0wQAYIKwYBBQUHAgIwNB4yIB0ATABlAGcAYQBsAF8A
# UABvAGwAaQBjAHkAXwBTAHQAYQB0AGUAbQBlAG4AdAAuIB0wDQYJKoZIhvcNAQEL
# BQADggIBAAfmiFEN4sbgmD+BcQM9naOhIW+z66bM9TG+zwXiqf76V20ZMLPCxWbJ
# at/15/B4vceoniXj+bzta1RXCCtRgkQS+7lTjMz0YBKKdsxAQEGb3FwX/1z5Xhc1
# mCRWS3TvQhDIr79/xn/yN31aPxzymXlKkVIArzgPF/UveYFl2am1a+THzvbKegBv
# SzBEJCI8z+0DpZaPWSm8tv0E4XCfMkon/VWvL/625Y4zu2JfmttXQOnxzplmkIz/
# amJ/3cVKC5Em4jnsGUpxY517IW3DnKOiPPp/fZZqkHimbdLhnPkd/DjYlPTGpQqW
# hqS9nhquBEKDuLWAmyI4ILUl5WTs9/S/fmNZJQ96LjlXdqJxqgaKD4kWumGnEcua
# 2A5HmoDF0M2n0O99g/DhO3EJ3110mCIIYdqwUB5vvfHhAN/nMQekkzr3ZUd46Pio
# SKv33nJ+YWtvd6mBy6cJrDm77MbL2IK0cs0d9LiFAR6A+xuJKlQ5slvayA1VmXqH
# czsI5pgt6o3gMy4SKfXAL1QnIffIrE7aKLixqduWsqdCosnPGUFN4Ib5KpqjEWYw
# 07t0MkvfY3v1mYovG8chr1m1rtxEPJdQcdeh0sVV42neV8HR3jDA/czmTfsNv11P
# 6Z0eGTgvvM9YBS7vDaBQNdrvCScc1bN+NR4Iuto229Nfj950iEkSoYICzjCCAjcC
# AQEwgfihgdCkgc0wgcoxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9u
# MRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29ycG9yYXRp
# b24xJTAjBgNVBAsTHE1pY3Jvc29mdCBBbWVyaWNhIE9wZXJhdGlvbnMxJjAkBgNV
# BAsTHVRoYWxlcyBUU1MgRVNOOkQ2QkQtRTNFNy0xNjg1MSUwIwYDVQQDExxNaWNy
# b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNloiMKAQEwBwYFKw4DAhoDFQCAoQnHMg20
# k9SbDi9ioy0ekLIqxqCBgzCBgKR+MHwxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpX
# YXNoaW5ndG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQg
# Q29ycG9yYXRpb24xJjAkBgNVBAMTHU1pY3Jvc29mdCBUaW1lLVN0YW1wIFBDQSAy
# MDEwMA0GCSqGSIb3DQEBBQUAAgUA4EDrvzAiGA8yMDE5MDMyNDAxMzAzOVoYDzIw
# MTkwMzI1MDEzMDM5WjB3MD0GCisGAQQBhFkKBAExLzAtMAoCBQDgQOu/AgEAMAoC
# AQACAiODAgH/MAcCAQACAhF/MAoCBQDgQj0/AgEAMDYGCisGAQQBhFkKBAIxKDAm
# MAwGCisGAQQBhFkKAwKgCjAIAgEAAgMHoSChCjAIAgEAAgMBhqAwDQYJKoZIhvcN
# AQEFBQADgYEAFaZxn+BLQbcN8affEbZrgwX2BOlljtdeqbcibhQxUj53u1NUHTIU
# Kie6a5Bge+4Vf5GtExjQTZ18PuaYdp68sgu+SkVqAxLOoBQGcLPusf15jZD4QlqX
# RtGx4t0LRmR+6XQku7FOS876kuKM1nqNTV7bd+RwmX7Wkg2fGcbbc1gxggMNMIID
# CQIBATCBkzB8MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
# A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSYw
# JAYDVQQDEx1NaWNyb3NvZnQgVGltZS1TdGFtcCBQQ0EgMjAxMAITMwAAAPV0GzyK
# Fyt87QAAAAAA9TANBglghkgBZQMEAgEFAKCCAUowGgYJKoZIhvcNAQkDMQ0GCyqG
# SIb3DQEJEAEEMC8GCSqGSIb3DQEJBDEiBCB69cfEvP2RlkxUchJqhXIZQb3XUbAK
# mx3OrBsjTUeudTCB+gYLKoZIhvcNAQkQAi8xgeowgecwgeQwgb0EILmemUJfoPpw
# w3NFNSJHKJlswrJjo22wl6nPQvZ8kx9wMIGYMIGApH4wfDELMAkGA1UEBhMCVVMx
# EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAcBgNVBAoT
# FU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjEmMCQGA1UEAxMdTWljcm9zb2Z0IFRpbWUt
# U3RhbXAgUENBIDIwMTACEzMAAAD1dBs8ihcrfO0AAAAAAPUwIgQgDdhPYHbPu5ld
# Tuvcy1Gc2F7AVGKm+Tyu4xi9z8mxmbwwDQYJKoZIhvcNAQELBQAEggEAX+xiHHlu
# wptXjT0Rnp4XZFfqyu9I1i6iGZQdFSwbs4FsXJVMPRpqmIHarbcSn3dO3LdpgzzN
# R9RoF4JYCof/YqFWrLKVGdnhv5WaY0D3LwR/YEU/iWR0N5p4Umwv+NXKt73HI6+r
# Uo6SkZET66gx1yRlazQRfQjpHygntp9eIqSCL7ixMnudp6OrDuR13R8FOWG0IYK4
# 7QBwIQyvVK9eDmdG7lF5wBj8ZbsTbT8qOievWIiY+swIOkzc2SZndviJVWWm+gl2
# CRYZhXRfeN9UB3Hb8w0yh/0pC42LREDhVoUJ/CWN5HPxnNnZikQcWeaTASONeUBt
# 3skyiX8qQZkVow==
# SIG # End signature block
